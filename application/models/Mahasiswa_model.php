<?php
class Mahasiswa_model extends CI_Model
{

  public function pasien()
    {
    $this->db->from('pasien');
		$query=$this->db->get();
		return $query->result();
    }

   function save_pasien($kode,$nama,$no_telepon,$tanggal,$alamat,$kelamin,$tempat,$ttl){
    $data = array(
      'kode' => $kode,
      'nama' => $nama,
      'no_telepon' => $no_telepon,
      'tanggal' => $tanggal,
      'alamat' => $alamat,
      'kelamin' => $kelamin,
      'tempat' => $tempat,
      'ttl' => $ttl
    );
    $this->db->insert('pasien',$data);
  }

    function edit_pasien($id){
    $query = $this->db->get_where('pasien', array('id' => $id));
    return $query;
}	

	function update_pasien($id,$kode,$nama,$no_telepon,$tanggal,$alamat,$kelamin,$tempat,$ttl){
    $data = array(
      'kode' => $kode,
      'nama' => $nama,
      'no_telepon' => $no_telepon,
      'tanggal' => $tanggal,
      'alamat' => $alamat,
      'kelamin' => $kelamin,
      'tempat' => $tempat,
      'ttl' => $ttl
    );
    $this->db->where('id', $id);
    $this->db->update('pasien', $data);
}

function perawatan_pasien($id){
    $query = $this->db->get_where('pasien', array('id' => $id));
    return $query;
}

function save_perawatan($id_mahasiswa,$tindakan,$kode_gigi,$tanggal,$diagnosa,$keluhan,$id_dosen,$bulan,$id_pasien){
    $data = array(
      'id_dosen' => $id_dosen,
      'id_mahasiswa' => $id_mahasiswa,
      'id_pasien' => $id_pasien,
      'tindakan' => $tindakan,
      'kode_gigi' => $kode_gigi,
      'tanggal' => $tanggal,
      'diagnosa' => $diagnosa,
      'keluhan' => $keluhan,
      'bulan' => $bulan
      
    );
    $this->db->insert('perawatan',$data);
  }

  public function laporan()
    {
    $this->db->select('perawatan.id,perawatan.id_mahasiswa, perawatan.id_dosen, perawatan.diagnosa,perawatan.keluhan, perawatan.kode_gigi as gigi, perawatan.tindakan, perawatan.tanggal, dosen.nama as dosen, diagnosa.nama as diagnosaa, mahasiswa.nama as mahasiswa, pasien.kode as kode_pasien, pasien.nama as nama_pasien');
    $this->db->from('perawatan');
    $this->db->join('pasien','pasien.id=perawatan.id_pasien');
    $this->db->join('mahasiswa','mahasiswa.id=perawatan.id_mahasiswa');
    $this->db->join('dosen','dosen.id=perawatan.id_dosen');
    $this->db->join('diagnosa','diagnosa.id=perawatan.diagnosa');
    $this->db->where('id_mahasiswa',$this->session->userdata('ses_id'));
		$query=$this->db->get();
		return $query->result();
    }

    public function dosen()
    {
    $this->db->from('dosen');
    $this->db->where('level', '2');
    $this->db->where('nama is NOT NULL', NULL, FALSE);
    $query=$this->db->get();
    return $query->result();
    }

    public function diagnosa()
    {
    $this->db->from('diagnosa');
    $query=$this->db->get();
    return $query->result();
    }


    function get_kode_gigi($kode){
    $hsl=$this->db->query("SELECT * FROM gigi WHERE kode_gigi='$kode'");
    if($hsl->num_rows()>0){
      foreach ($hsl->result() as $data) {
        $hasil=array(
          'kode_gigi' => $data->kode_gigi,
          'deskripsi' => $data->deskripsi,
          );
      }
    }
    return $hasil;
  }

  function get_kode_diagnosa($kodee){
    $hsl=$this->db->query("SELECT * FROM diagnosa WHERE id='$kodee'");
    if($hsl->num_rows()>0){
      foreach ($hsl->result() as $data) {
        $hasil=array(
          'id' => $data->id,
          'kode' => $data->kode,
          'nama' => $data->nama,
          );
      }
    }
    return $hasil;
  }

   public function ubah_password()
    {
    $this->db->from('mahasiswa');
    $query=$this->db->get();
    return $query->result();
    }

    public function kode_gigi_anak(){
      return $this->db->get('kode_gigi_anak')->result_array();
    }

    public function kode_gigi_dewasa(){
      return $this->db->get('kode_gigi_dewasa')->result_array();
    }

    public function save()
 {
      $pass = md5($this->input->post('new'));
      $data = array (
       'password' => $pass
       );
      $this->db->where('id', $this->session->userdata('ses_id'));
      $this->db->update('mahasiswa', $data);
 }

 public function cek_old()
  {
   $old = md5($this->input->post('old')); 
   $this->db->where('password',$old);
   $query = $this->db->get('mahasiswa');
    return $query->result();;
}
function profile($id){
    $query = $this->db->get_where('mahasiswa', array('id' => $id));
    return $query;
}
function update_profile($id,$nama,$nim,$tempat,$ttl,$alamat,$no_telepon,$tanggal){
    $data = array(
      'nama' => $nama,
      'nim' => $nim,
      'tempat' => $tempat,
      'ttl' => $ttl,
      'alamat' => $alamat,
      'no_telepon' => $no_telepon,
      'tanggal' => $tanggal
    );
    $this->db->where('id', $id);
    $this->db->update('mahasiswa', $data);
}

public function update($data,$kondisi)
  {
      $this->db->update('mahasiswa',$data,$kondisi);
      return TRUE;
  }

public function show_profile()
    {
    $this->db->from('mahasiswa');
    $this->db->where('id', $this->session->userdata('ses_id'));
    $query=$this->db->get();
    return $query->result();
    }


  public function grafik()
    {
    $tahun = date("Y");
    //$bulan = "select month(tanggal) as bulan from perawatan";

    $this->db->where('year(perawatan.tanggal)', $tahun);
    $this->db->where('id_mahasiswa', $this->session->userdata('ses_id') );
    $this->db->select('bulan');
    $this->db->select("count(*) as total");
    $this->db->group_by('bulan');
    $this->db->order_by('id');
    
    return $this->db->from('perawatan')
    ->get()
    ->result();
    }

    public function kode(){
      $this->db->select('RIGHT(pasien.kode,2) as kode', FALSE);
      $this->db->order_by('kode','DESC');    
      $this->db->limit(1);    
      $query = $this->db->get('pasien');  //cek dulu apakah ada sudah ada kode di tabel.    
      if($query->num_rows() <> 0){      
         //cek kode jika telah tersedia    
         $data = $query->row();      
         $kode = intval($data->kode) + 1; 
      }
      else{      
         $kode = 1;  //cek jika kode belum terdapat pada table
      }
        $tgl=date('dmY');
        $id= $this->session->userdata('ses_id');
        $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
        $kodetampil = "JKG".$id.$batas;  //format kode
        return $kodetampil;  
     }



 
}